"""
Black Biss - Advance Python

Write a function that calculate how many month one should save money to buy new car.
"""


def home_economics(startPriceOld, startPriceNew, savingPerMonth, percentLossByMonth):
    if startPriceNew <= startPriceOld:
        ret1 = (0, (startPriceOld-startPriceNew))
        return ret1
    month = 0
    while True:
        month += 1
        if month % 2 == 0 and month >= 2:
            percentLossByMonth = percentLossByMonth + 0.5
        startPriceOld = startPriceOld * (100 - percentLossByMonth)/100
        startPriceNew = startPriceNew * (100 - percentLossByMonth)/100
        money = startPriceOld + savingPerMonth * month
        if money >= startPriceNew:
            ret1 = (month, round(money - startPriceNew))
            return ret1


# small test to check it's work.
if __name__ == '__main__':

    ret = home_economics(2000, 8000, 1000, 1.5)
    if ret[0] == 6 and ret[1] == 766:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")
