"""
Black Biss - Advance Python

Write a function that get a path to file that encrypted with 3 english-small-letter xor key, and print the
deciphered text.
"""

def crypto_breaker():
    for c in range(26):
        for j in range(26):
            for i in range(26):
                crypto_breaker1("big_secret.txt", 97+i, 97 + j, 97 + c)


def crypto_breaker1(file_path, key1, key2, key3):
    with open(file_path) as text_file:
        text = text_file.read().split(",")
    i = 0
    while i < len(text):
        text[i] = str(int(text[i]) ^ key1)
        i += 1
        text[i] = str(int(text[i]) ^ key2)
        i += 1
        text[i] = str(int(text[i]) ^ key3)
        i += 1
    for i in range(len(text)):
        text[i] = chr(int(text[i]))
    text = "".join(text)
    if text.__contains__("and") and text.__contains__(" a "):
        print(chr(key1) + " " + chr(key2) + " " + chr(key3))
        print(text)


# small test to check it's work.
if __name__ == '__main__':
    crypto_breaker()
