"""
Black Biss - Advance Python

Write a function that read one line from the user with many potentional passwords seperated by comma ","
and print out only the valid password - by the following rules:
    * contain letter from each of the characters-sets:
        lower-case english, upper-case english, decimal digit,
        special characters *&@#$%^
    * dosn't contain other symbols (ignore white-spaces)
    * length between 6 and 12

"""


def lower(temp):
    for i in range(len(temp)):
        if 97 <= ord(temp[i]) <= 122:
            return True
    return False

def upper(temp):
    for i in range(len(temp)):
        if 65 <= ord(temp[i]) <= 90:
            return True
    return False


def numbers(temp):
    for i in range(len(temp)):
        if 48 <= ord(temp[i]) <= 57:
            return True
    return False


def special(temp):
    for i in range(len(temp)):
        if 33 <= ord(temp[i]) <= 43 or ord(temp[i]) == 64:
            return True
    return False


def validate_passwords():
    with open("input_passwords.txt") as text_file:
        passwords = text_file.read().split()
    for i in range(len(passwords)):
        passwords[i] = passwords[i].replace(",", "")
    for i in range(len(passwords)):
        if special(passwords[i]) and upper(passwords[i]) and lower(passwords[i]) and numbers(passwords[i]) and\
                12 > len(passwords[i]) > 6:
            print(passwords[i])


# small test to check it's work.
if __name__ == '__main__':
    validate_passwords()
