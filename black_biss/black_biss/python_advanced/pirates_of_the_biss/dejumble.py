"""
Black Biss - Advance Python

Write a function that suggest which word is the Pirates meant to write.
"""


def dejumble(word, potentional_words):
    current_word = ""
    correct_words = []
    j = 0
    while j < len(potentional_words):
        for i in range(len(word)):
            if word[i] in potentional_words[j]:
                current_word = current_word + potentional_words[j][i]
        if len(current_word) == len(word):
            correct_words.append(current_word)
        j += 1
        current_word = ""
    return correct_words


# small test to check it's work.
if __name__ == '__main__':

    ret = dejumble("orspt", ["sport", "parrot", "ports", "matey"])
    print(ret)
    if len(ret) == 2 and set(ret) == set(["sport", "ports"]):
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")
