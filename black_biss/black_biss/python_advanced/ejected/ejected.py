import math

"""
Black Biss - Advance Python

Write a function that calculate how far is our ejected pilot.
Run this program and insert the following lines (each end with Enter):

UP 5
DOWN 3
LEFT 3
RIGHT 2
0

and the result should be 2
"""


def ejected():
    with open("input_ejected.txt") as text_file:
        commands = text_file.read().split()
    i = 0
    y = 0
    x = 0
    while i < len(commands):
        if commands[i] == "0":
            temp = round(math.sqrt(math.pow(x, 2) + math.pow(y, 2)))
            return temp
        if commands[i] == "UP":
            i += 1
            y = y + int(commands[i])
        elif commands[i] == "DOWN":
            i += 1
            y = y - int(commands[i])
        elif commands[i] == "LEFT":
            i += 1
            x = x - int(commands[i])
        else:
            i += 1
            x = x + int(commands[i])
        i += 1


# small test to check it's work.
if __name__ == '__main__':
    print(ejected())
