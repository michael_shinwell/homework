"""
Black Biss - Advance Python

Write a function that get array of string, each represent a row in 2-dim map, when:
    'X' is start / destination point
    '-' is road right-left
    '|' is road up-down
    '+' is a turn in the road, can be any turn (which isn't continue straight)
    path = ["           ",
            "X-----+    "
            "      |    "
            "      +---X",
            "           "]

    # Note: this grid is only valid when starting on the right-hand X, but still considered valid
    path = ["                      ",
            "   +-------+          ",
            "   |      +++---+     ",
            "X--+      +-+   X     "]
    nevigate_validate(path)  # ---> True
"""


def map(path, last, current):
    if path[current[0]][current[1]] == "X":
        return 1
    elif path[current[0]][current[1]] == "-":
        new = (current[0], current[1]+(current[1]-last[1]))
        return map(path, current, new)
    elif path[current[0]][current[1]] == "|":
        new = (current[0] + (current[0]-last[0]), current[1])
        return map(path, current, new)
    elif path[current[0]][current[1]] == "+":
        if current[0] == last[0]:
            new = (current[0] + 1, current[1])
            new1 = (current[0] - 1, current[1])
            return map(path, current, new) + map(path, current, new1)
        else:
            new = (current[0], current[1] + 1)
            new1 = (current[0], current[1] - 1)
            return map(path, current, new) + map(path, current, new1)
    return 0


def findx(path):
    for j in range(len(path)):
        for i in range(len(path[0])):
            if path[j][i] == "X":
                return j, i


def nevigate_validate(path):
    point = findx(path)
    counter = 0
    j = point[0]
    i = point[1]
    try:
        counter = counter + map(path, (j, i), (j, i - 1))
        counter = counter + map(path, (j, i), (j, i + 1))
        counter = counter + map(path, (j, i), (j + 1, i))
        counter = counter + map(path, (j, i), (j - 1, i))
    except:
        return False
    print(counter)
    if counter == 1:
        return True
    return False



#small test to check it's work.
if __name__ == '__main__':
    path = ["           ",
            "X-----+    ",
            "      |    ",
            "      +---X",
            "           "]
    valid = nevigate_validate(path)
    print(valid)
    path = ["           ",
            "X--|--+    ",
            "      -    ",
            "      +---X",
            "           "]
    invalid = nevigate_validate(path)
    if valid and not invalid:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")
