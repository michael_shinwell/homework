

def camelChange(word):
    temp = ""
    for i in range(len(word)-1):
        temp = temp + word[i]
        if word[i+1].isupper():
            temp = temp + " "
    temp = temp + word[len(word)-1]
    return temp


if __name__ == '__main__':
    camel = "camelCaseTest"
    test = "camel Case Test"
    print(camelChange(camel))
    if camelChange(camel) == test:
        print("success")
