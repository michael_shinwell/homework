"""
Black Biss - Basic Python

Write a function that replace any second word with the word "Corona".
"""


def word_pandemic(word):
    i = 0
    new_word = ""
    num = 1
    while i < len(word):
        if word[i] == " ":
            new_word = new_word + " Corona"
            i = i + 1
            while word[i] != " ":
                i = i + 1
        new_word = new_word + word[i]
        i = i + 1
    return new_word


# small test to check it's work.
if __name__ == '__main__':
    base_word = "Koala Bears are the cutest"
    sick_word = "Koala Corona are Corona cutest"
    if word_pandemic(base_word) == sick_word:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")
