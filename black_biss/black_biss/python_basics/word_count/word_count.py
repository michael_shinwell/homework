import sys


def words_to_dic(filename):
    with open(filename) as text_file:
        text = text_file.read().lower().split()
    word_counts = dict()
    for word in text:
        if word in word_counts:
            word_counts[word] = word_counts[word] + 1
        else:
            word_counts[word] = 1
    return word_counts


def print_words(filename):
    word_counts = words_to_dic(filename)
    items = word_counts.items()
    sort = sorted(items)
    print(sort)


def print_top(filename):
    word_counts = words_to_dic(filename)
    word_counts = sorted(word_counts.items(), key = lambda x: x[1] , reverse=True)
    for i in range(20):
        print(word_counts[i])


def main():
    if len(sys.argv) != 3:
        print_top("alice_in_wonderland.txt")
        sys.exit(1)
    option = sys.argv[1]
    filename = sys.argv[2]
    if option == '--count':
        print_words(filename)
    elif option == '--topcount':
        print_top(filename)

    else:
        print("unknown option: " + option)
        sys.exit(1)


if __name__ == '__main__':
    main()
