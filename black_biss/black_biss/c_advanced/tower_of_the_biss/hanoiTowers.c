#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


void towers(int discs, char origin, char target, char middle) {
    if (discs == 1) {
        printf("move disk from %c to %c\n", origin, target);
        return;
    }
    towers(discs - 1, origin, middle, target);
    printf("move disk from %c to %c\n", origin, target);
    towers(discs - 1, middle, target,origin);

    
}


int main() {
    towers(3, 'a', 'c', 'b');
    return 0;
}