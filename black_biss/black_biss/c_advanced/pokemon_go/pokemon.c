#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


//struct Books{
//   char  title[50];
//   char  author[50];
//   char  subject[100];
//   int   book_id;
//};
struct pokemon {
	double att;
	double def;
	char type[6];
	char name[20];
};

void battle(struct pokemon own, struct pokemon enemy) {
	int effective = 0;
	if (own.type[0] == 'f') {
		if (enemy.type[0] == 'w')
			effective = 1;
		else if (enemy.type[0] == 'e' || enemy.type[0] == 'f')
			effective = 2;
		else
			effective = 3;
			
	}
	else if (own.type[0] == 'w') {
		if (enemy.type[0] == 'e')
			effective = 1;
		else if (enemy.type[0] == 'a' || enemy.type[0] == 'w')
			effective = 2;
		else
			effective = 3;
	}
	else if (own.type[0] == 'e') {
		if (enemy.type[0] == 'a')
			effective = 1;
		else if (enemy.type[0] == 'e' || enemy.type[0] == 'f')
			effective = 2;
		else
			effective = 3;
	}
	else {
		if (enemy.type[0] == 'f')
			effective = 1;
		else if (enemy.type[0] == 'a' || enemy.type[0] == 'w')
			effective = 2;
		else
			effective = 3;
	}
	double temp = fabs(effective - 4);
	double stat1 = own.att / enemy.def * effective;
	double stat2 = enemy.att / own.def * temp;
	if (stat2 > stat1)
		printf("%s wins",enemy.name);
	else
		printf("%s wins",own.name);
}

int main() {
	struct pokemon own;
	own.att = 30;
	own.def = 40;
	strcpy(own.type, "earth");
	strcpy(own.name, "charmander");
	struct pokemon enemy;
	enemy.att = 40;
	enemy.def = 50;
	strcpy(enemy.type, "water");
	strcpy(enemy.name, "squirtle");
	battle(own, enemy);
	return 0;
}