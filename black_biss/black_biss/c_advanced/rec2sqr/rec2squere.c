#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

struct rec {
	int height;
	int width;
};

void rec2sqr(struct rec rectangle) {
	if (rectangle.height == rectangle.width)
		printf("%d ", rectangle.height);
	else 
		while (rectangle.height != 0 && rectangle.width != 0) {
			if (rectangle.height > rectangle.width) {
				printf("%d ", rectangle.width);
				rectangle.height = rectangle.height - rectangle.width;
			}
			else {
				printf("%d ", rectangle.height);
				rectangle.width = rectangle.width - rectangle.height;
			}				
		}
}


int main() {
	struct rec rec1;
	rec1.height = 3;
	rec1.width = 5;
	rec2sqr(rec1);
	return 0;
}