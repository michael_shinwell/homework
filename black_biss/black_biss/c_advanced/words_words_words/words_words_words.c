#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int exceptions(int num) {
	int counter = 0;
	if (num == 10)
		return 3;
	int digit = num % 10;
	if (digit == 3 || digit == 8 || digit == 9 || digit == 4)
		counter = counter + 8;
	else if (digit == 5 || digit == 6)
		counter += 7;
	else if (digit == 7)
		counter += 9;
	else
		counter = 6;
	return counter;
}



int convertNum(int num) {
	int counter=0;
	if (num > 9 && num < 20)
		return exceptions(num);
	if (num == 100)
		return 10;
	int digit = num % 10;
	if (digit == 1 || digit == 2 || digit == 6)
		counter = counter + 3;
	else if (digit == 3 || digit == 4 || digit == 5 || digit == 9)
		counter += 4;
	else if (digit == 8 || digit == 7)
		counter += 5;
	num = num / 10;
	if (num != 0) {
		digit = num;
		if (digit == 2 || digit == 3 || digit == 8)
			counter = counter + 6;
		else if (digit == 4 || digit == 5 || digit == 6)
			counter += 5;
		else
			counter += 7;
	}
	return counter;
	
}

int main() {
	int count=0;
	for (int i = 1; i <= 100; i++)
		count = count + convertNum(i);
	printf("%d", count);
	return 0;
}