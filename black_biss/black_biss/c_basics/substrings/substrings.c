/**
 * @file substrings.c
 * @version 1.0
 * @author ????????
 *
 * @brief program that check if one string is substring of the other and return pointer
 * to the start of the substring in the string.
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 */

// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>

// ------------------------------ functions -----------------------------
char * my_strstr(const char * a, const char * b)
{
	int i;
	if (strlen(a) > strlen(b)) {
		for (int j = 0; j < strlen(a) - strlen(b); j++) {
			i = 0;
			while (i < strlen(b)) {
				if (a[j + i] != b[i]) 
					break;
				i++;
			}
			if (i == strlen(b))
				return b;
		}
	}
	else {
		for (int j = 0; j < strlen(b) - strlen(a); j++) {
			i = 0;
			while (i < strlen(a)) {
				if (b[j + i] != a[i])
					break;
				i++;
			}
			if (i == strlen(a))
				return a;
		}
	}

	return NULL;
}

int main2(int argc, char * argv[])
{
   char s1[] = "AAAAAAACCCAAAAAAAA";
   char s2[] = "CCC";

   char * ret = my_strstr(s1, s2);

   printf("The substring is: %s\n", ret);

	return 0;
}