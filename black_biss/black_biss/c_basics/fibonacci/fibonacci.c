/**
 * @file fibonacci.c
 * @version 1.0
 * @author ????????
 *
 * @brief program that calculate the n'th number of fibonacci sequence.
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 * @section DESCRIPTION
 * The system get number n as argument and find the n'th value in fibonacci sequence.
 * Input  : n - number
 * Output : print out the n'th value in fibonacci sequence.
 */


// ------------------------------ includes ------------------------------

#include <stdio.h>
#include <stdlib.h>

// ------------------------------ functions -----------------------------
int find_nth_fibonacci(int n)
{	
    return temp(n, 1, 2);
}

int temp(int n, int num1, int num2) {
    if (n == 4)
        return num1 + num2;
    if (num1 > num2)
        return temp(n - 1, num1, num2 + num1);
    return temp(n - 1, num2 + num1, num2);
}

int mainq()
{
    int n = 5;
    int num = find_nth_fibonacci(n);
    printf("The %d'th number of fibonacci sequence is %u.\n", n, num);

	return 0;
}
