#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int val;
    struct node* next;
} node_t;


void insert(int n,int value, node_t*chain) {
    for (int i = 0; i < n; i++) {
        if (chain->next == NULL) {
            printf("no value at requested index");
            return 0;
        }
    }
    node_t*newchain = (node_t*)malloc(sizeof(node_t));
    newchain->val = value;
    node_t* temp = chain->next;
    chain->next = newchain;
    newchain->next = temp;
}

int main() {
    node_t*head = (node_t*)malloc(sizeof(node_t));
    node_t* temp = head;
    for (int i = 0; i < 5; i++) {        
        head->val = 1;
        head->next = (node_t*)malloc(sizeof(node_t));
        head = head->next;
    }
    head = temp;
    insert(2, 2, head);
    while (head != NULL) {
        printf(" %d ", head->val);
        head = head->next;
    }
    return 0;
}