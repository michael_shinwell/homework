#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>


void func(char code[], int i) {
    char code2[sizeof(code)];
    strcpy(code2, code);
    if (i != sizeof(code)) {
        printf("%s\n", code);
        func(code, i + 1);
        strcpy(code, code2);
        switch (code[i])
        {
        case '1':
            code[i] = '2';
            func(code, i + 1);
            strcpy(code, code2);
            code[i] = '4';
            func(code, i + 1);
            break;
        case '2':
            code[i] = '1';
            func(code, i + 1);
            strcpy(code, code2);
            code[i] = '3';
            func(code, i + 1);
            strcpy(code, code2);
            code[i] = '5';
            func(code, i + 1);
            break;
        case '3':
            code[i] = '2';
            func(code, i + 1);
            strcpy(code, code2);
            code[i] = '6';
            func(code, i + 1);
            break;
        case '4':
            code[i] = '1';
            func(code, i + 1);
            strcpy(code, code2);
            code[i] = '5';
            func(code, i + 1);
            strcpy(code, code2);
            code[i] = '7';
            func(code, i + 1);
            break;
        case '5':
            code[i] = '2';
            func(code, i + 1);
            strcpy(code, code2);
            code[i] = '4';
            func(code, i + 1);
            strcpy(code, code2);
            code[i] = '6';
            func(code, i + 1);
            strcpy(code, code2);
            code[i] = '8';
            func(code, i + 1);
            break;
        case '6':
            code[i] = '3';
            func(code, i + 1);
            strcpy(code, code2);
            code[i] = '9';
            func(code, i + 1);
            strcpy(code, code2);
            code[i] = '5';
            func(code, i + 1);
            break;
        case '7':
            code[i] = '4';
            func(code, i + 1);
            strcpy(code, code2);
            code[i] = '8';
            func(code, i + 1);
            break;
        case '8':
            code[i] = '5';
            func(code, i + 1);
            strcpy(code, code2);
            code[i] = '7';
            func(code, i + 1);
            strcpy(code, code2);
            code[i] = '9';
            func(code, i + 1);
            strcpy(code, code2);
            code[i] = '0';
            func(code, i + 1);
            break;
        case '9':
            code[i] = '6';
            func(code, i + 1);
            strcpy(code, code2);
            code[i] = '8';
            func(code, i + 1);
            break;
        case '0':
            code[i] = '8';
            func(code, i + 1);
            break;

        default:
            break;
        }
       
    }
}



int main() { 
    char arr[4][3] = {
    {'1', '2', '3'},
    {'4', '5', '6'},
    {'7', '8', '9'},
    {'*', '0', '#'}
    };
    char code[] = "113";
    func(code, 0);
    return 0;
}