/**
 * @file word_count.c
 * @version 1.0
 * @author ????????
 *
 * @brief program that print how many words are in a given file.
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 * @section DESCRIPTION
 * The system get file path as argument. Open it and count the word's in it.
 * Input  : file_path
 * Output : print out the amount of words.
 */


// ------------------------------ includes ------------------------------
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>


// ------------------------------ functions -----------------------------
int main()
{
    int word_count = 0;  
    int c;
    FILE* file;
    file = fopen("aladdin.txt", "r");
   
    while ((c = getc(file)) != EOF)
        if (c == 32 || c == 95)
            word_count++;
    fclose(file);
    
    printf("The file contain %u words.\n", word_count);

	return 0;
}